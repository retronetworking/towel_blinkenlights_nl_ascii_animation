<?php

$frames = json_decode(file_get_contents("blinkenlights.json"), true);

$last_timestamp = 0;
foreach ($frames as $timestamp => $frame)
{
	echo chr(27) . $frame;
	usleep(($timestamp - $last_timestamp) * 1000);
	$last_timestamp = $timestamp;
}