<?php
$start = microtime(true);
$socket = fsockopen("towel.blinkenlights.nl", 23);

$animation = [];
$currentframe = "";

if (!$socket) return;
stream_set_blocking($socket, 0);
stream_set_blocking(STDIN, 0);

do {
  $read   = array( $socket, STDIN); $write  = NULL; $except = NULL;

  if(!is_resource($socket)) return;
  $num_changed_streams = @stream_select($read, $write, $except, null);
  if(feof($socket)) return ;

  if($num_changed_streams  === 0) continue;
  if (false === $num_changed_streams) {
      /* Error handling */
    var_dump($read);
    echo "Continue\n";
    die;
  } elseif ($num_changed_streams > 0) {
    echo "\r";
    $data = fread($socket, 16);
    if($data !== "") 
    {
      $currentframe .= $data;

      if (strpos($currentframe, chr(27)) !== FALSE)
      {
        $time = (int)((microtime(true) - $start) * 1000.0);
        $animation[$time] = explode(chr(27), $currentframe, 2)[0];
        $currentframe = explode(chr(27), $currentframe, 2)[1];

        file_put_contents("blinkenlights.json", json_encode($animation, JSON_PRETTY_PRINT));
      }
    }
  }

} while(true);
